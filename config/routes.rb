Rails.application.routes.draw do
  devise_for :admins

  scope :api,
        constraints: { subdomain: ENV['TRADER_API_SUBDOMAIN'] },
        defaults: { format: :json } do
    scope :v1 do
      devise_for :traders
      scope '/', module: 'api', as: :trader do
        post :connect, to: 'api#connect'
      end
    end
  end

  scope :api,
        constraints: { subdomain: ENV['RETAILER_API_SUBDOMAIN'] },
        defaults: { format: :json } do
    scope :v1 do
      devise_for :retailers
      scope '/', module: 'api', as: :retailer do
        post :connect, to: 'api#connect'
      end
    end
  end
end
