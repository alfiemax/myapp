class API::APIController < ActionController::API
  before_action :authenticate_trader!

  respond_to :json

  def connect
    render nothing: true
  end
end
