class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :jwt_authenticatable, jwt_revocation_strategy: self

  validates :type, presence: true
  validates :email, presence: true,
                    uniqueness: { scope: :type }
  validates :password, presence: true, on: :create
  validates :password, presence: true, on: :update, allow_blank: true

  has_many :connections, dependent: :destroy
  has_many :connected_users, through: :connections

  def jwt_payload
    super
  end
end
