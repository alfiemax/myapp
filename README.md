# TraderApp

#### Rails App
    - Ruby 2.4.1
    - Rails 5.1.1
    - PostgreSQL > 9.5

#### Development Setup
    Create following config files (use example files present in the config dir)
      - config/application.yml
      - config/databse.yml
      - config/secrets.yml

    Required ENV Vars
      - DEVISE_JWT_SECRET_KEY
      - TRADER_API_SUBDOMAIN
      - RETAILER_API_SUBDOMAIN

    Setup DB
      Execute : `rake db:create db:migrate db:seed`

    Start Rails App
      Execute : `bundle exec rails server`


#### Steps to reproduce the bug
    Setup the app
      All config files are commited, so just create the db and migrate and you are good to go.

    Edit etc/hosts to point your localhost to `trader.ta.com`

    Now start your rails app `rails s`

    Now, Create a trader user using this endpoint : POST trader.ta.com:3000/api/v1/traders

    ```
      {
        "trader" : {
          "email" : "trader@me.com",
          "password" : "password"
        }
      }
    ```

    Now use the Authorization header in the response to make the following request : POST trader.ta.com:3000/api/v1/connect

    You should get a success response now. Go ahead and send the request multiple times, all should work fine.

    Now this is the important part to reproduce the bug :

    Go ahead and edit any of your model or controller (just to trigger a code change)
    Yes, it doesn't matter which file you edit, just add a newline and that's it. Now send the same reqest again.

    It gives me this error :

    ```
    ActiveRecord::SubclassNotFound (Invalid single-table inheritance type: Trader is not a subclass of Trader)
    ```
